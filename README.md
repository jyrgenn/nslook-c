# nslook-c
simple, concise ip address and hostname lookup, started 1992;
status: complete, fairly stable

nslook
------

When I first wrote this in March 1992, I wanted a program to look up
IP addresses for hosts (and the reverse) that was more concise than
nslookup(1). Also, either I was not aware that for reverse lookups
you could give it just the IP address in dotted decimal form, or it
was actually necessary to do this dance:

    > set q=ptr
    > 88.199.214.85.in-addr.arpa

Anyway, I found that extremely tedious, and I also found nslookup's
output too verbose in most situation.

On top of that, in some cases nslookup and its younger siblings
host(1) and dig(1) weren't helpful debugging name lookup problems --
they always used the resolver library and thus DNS, which wasn't the
case for most other programs, which used gethostbyname(3) and
gethostbyaddr(3). Depending on the system's configuration, this
resulted in DNS lookups or not, and at least for the cases where
not, I needed some other tool to isolate and reproduce an issue
using the standard name lookup whatever it was -- DNS, files, or
NIS, to name a few.

So I wrote nslook(1). The name as a shortened form of "nslookup"
should express what I wanted, a more concise and in some situations
more convenient program to look up names and addresses "the normal
way", for whatever values of normal your system had.

The program proved to be useful, and while I tinkered with it a bit
a few times in the following 20 years, I never made bigger changes.
In particular I never made it support the new IPv6 API. I thought
about it some times, but was too lazy to go for this change.


End of 2013 I recognized that I needed something exactly like this
program again. With that new motivation, I finally made it use the
new IPv6 API, which has been present in the relevant platforms for a
number of years now. This resulted in some larger changes, but it
was still easy to do -- the overall structure of the program was
more sound than I had expected from something in my earlier years as
a programmer.

Some examples:

    $ nslook ribe.w21.org
    85.214.199.88
    2a01:238:4382:a300:3fe5:41ce:8574:8378

    $ nslook -4 ribe.w21.org
    85.214.199.88

    $ nslook -m ribe.w21.org
    85.214.199.88                            ribe.w21.org
    2a01:238:4382:a300:3fe5:41ce:8574:8378   ribe.w21.org

    $ nslook -l ribe.w21.org
    query:      ribe.w21.org
    ip address: 85.214.199.88
    ip address: 2a01:238:4382:a300:3fe5:41ce:8574:8378

    $ nslook -lc mail.w21.org
    query:      mail.w21.org
    hostname:   ribe.w21.org
    ip address: 85.214.199.88
    ip address: 2a01:238:4382:a300:3fe5:41ce:8574:8378

    $ nslook 85.214.199.88
    ribe.w21.org

    $ nslook -lr w21.org
    query:      w21.org
    ip address: 85.214.199.88
    reverse:    ribe.w21.org
    ip address: 2a01:238:4382:a300:3fe5:41ce:8574:8378
    reverse:    ribe.w21.org

If you feel particularly inquisitive, try the -x option with a
forward lookup.

The copyright claim and the license conditions in the LICENSE file
apply to all files of this package.

In the morgue/ subdirectory you can find a shar file of the program
from March 1992 and the original README file. Wow, those were the
days...

-- 
Juergen Nickelsen <ni@w21.org> 2015-04-13
