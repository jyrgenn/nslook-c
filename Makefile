# Copyright (C) 1992, 2013, 2014 Juergen Nickelsen <ni@w21.org>.
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

CC = cc
CFLAGS = -g -Wall -Wstrict-prototypes
# for most systems:
#LIBS = -lresolv

# Interactive:
# LIBS = -linet
# Solaris:
#LIBS = -lnsl -lsocket

INSTALLBASE = /opt/w21
SOURCES = README nslook.1 nslook.c Makefile

all: nslook nslook.ps

nslook : nslook.c version Makefile
	$(CC) $(CFLAGS) -o nslook nslook.c $(LIBS)

version:
	bdate=$$(date "+%Y%m%d:%H%M%S $$USER@$$HOST") ;\
	printf "#define VERSION \"nslook (c) 1992-2015 J. Nickelsen <ni@w21.org> build $$bdate\"\\n" > version.h

nslook.ps: nslook.1
	groff -man nslook.1 > nslook.ps

install:
	for i in bin share/man/man1; do mkdir -p $(INSTALLBASE)/$$i; done
	cp nslook $(INSTALLBASE)/bin
	cp nslook.1 $(INSTALLBASE)/share/man/man1

clean:
	rm -rf nslook *.o *~ core nslook.ps TAGS version.h nslook.dSYM
